# HAL_9000

Reproduction de HAL 9000 pour mon décor.

Basé sur une lentille de 104,5mm récupérée dans un agrandisseur

* Boite découpée à la laser dans du médium 3mm.
* Peinture à la bombe (couleur argentée et noir)
* Pour un rendu propre, le fond est découpé dans du vinyle noir.
* Le logo est découpé à la découpe vinyle dans du bleu clair et du blanc